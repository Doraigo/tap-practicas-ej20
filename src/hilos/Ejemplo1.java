package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejemplo1 implements Runnable{
    public static void main(String[] args) {
         java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ejemplo1().run();
            }
        });
        Hilo h1 =new Hilo("Tarea 1",500);
        Hilo h2 =new Hilo("Tarea 1",500);
        Hilo h3 =new Hilo("Tarea 1",500);
        
    }
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(1000);
                System.out.println("Ejecucion principal,contador: "+i);
            } catch (InterruptedException ex) {
                Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
class Hilo extends  Thread{
String id="";
int miliseg=1000;
    public Hilo(String id,int miliseg) {
    this.id = id;
    this.miliseg = miliseg;
    }
    
    
    @Override
    public void run() {
    for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(miliseg);
                System.out.printf("Ejecucion de tarea %s,contador: %d\n",this.id,i);
            } catch (InterruptedException ex) {
                Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
