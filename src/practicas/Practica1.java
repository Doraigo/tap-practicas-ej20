package practicas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Practica1 extends JFrame implements ActionListener{

    JLabel jl;
    JTextField jt;
    JButton jb;

    public Practica1() {

        this.setTitle("Practica 1");
        this.setSize(300, 150);
        this.setLayout(new FlowLayout());
        this.setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        jl = new JLabel("Escriba un nombre para saludar");
        jt = new JTextField(20);
        jb = new JButton("¡Saludar!");

        this.add(jl);
        this.add(jt);
        this.add(jb);
        jb.addActionListener(this);
    }
    
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica1().setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
    JOptionPane.showMessageDialog(this, "¡Hola "+jt.getText()+"!");
    }
}
